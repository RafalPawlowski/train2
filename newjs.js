(function () {

    var form = document.getElementsByTagName("form") [0];
    
    form.addEventListener('submit', function (event) {
        
        event.preventDefault();
        var input = document.getElementById("numbers");
        var numbersString = input.value;
        
        if (numbersString.trim() === "") {
            alert("Pole nie może być puste");
            return;
        }
        
        var numbers = numbersString.split(",");
        
        var sum = numbers.reduce(function (previousValue, currentValue) {
            return parseInt(previousValue) + parseInt(currentValue);
        });

        document.getElementById("average").innerHTML = "średnia wynosi: " + (sum / numbers.length);
    });
})();
